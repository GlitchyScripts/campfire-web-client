
$.ajaxTransport("+binary", function(options, originalOptions, jqXHR) {
	// Check for conditions and support for blob / arraybuffer response type.
	if (
		window.FormData && (
			(options.dataType && (options.dataType == 'binary'))
			|| (
				options.data
				&& (
					(window.ArrayBuffer && options.data instanceof ArrayBuffer)
					|| (window.Blob && options.data instanceof Blob)
				)
			)
		)
	) {
		return {
			// Create new XMLHttpRequest.
			send: function(headers, callback) {
				// Setup all variables.
				var xhr = new XMLHttpRequest();
				var url = options.url;
				var type = options.type;
				var async = options.async || true;
				// Can be blob or arraybuffer. Default is blob.
				var dataType = options.responseType || "blob";
				var data = options.data || null;
				var username = options.username || null;
				var password = options.password || null;
				
				xhr.addEventListener('load', function() {
					var data = {};
					data[options.dataType] = xhr.response;
					// Make callback and send data.
					callback(xhr.status, xhr.statusText, data, xhr.getAllResponseHeaders());
				});
				
				xhr.addEventListener('error', function(event) {
					// Make error callback.
					callback(xhr.status, xhr.statusText);
				});
				
				xhr.open(type, url, async, username, password);
				
				// Setup custom headers.
				for (var i in headers ) {
					xhr.setRequestHeader(i, headers[i]);
				}
				
				xhr.responseType = dataType;
				xhr.send(data);
			}
			, abort: function() {
				jqXHR.abort();
			}
		};
	}
});

function CampfireUtilParseHref(href)
{
	if (href.length <= 0) {
		return undefined;
	}
	
	let linkDatabase = undefined;
	if (href.substr(0, 1) === "/" && href.substr(1, 1) !== "/") {
		try {
			linkDatabase = new URL(new URL(window.location).origin + href);
			return linkDatabase;
		} catch (error) {
			return undefined;
		}
	}
	if (href.indexOf("//") === -1) {
		try {
			linkDatabase = new URL("https://" + href);
			return linkDatabase;
		} catch (error) {
			return undefined;
		}
	}
	
	try {
		linkDatabase = new URL(href);
		return linkDatabase;
	} catch (error) {
	}
	return undefined;
}